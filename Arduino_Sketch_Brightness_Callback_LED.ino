#include <SPI.h>

/*
 * Prof. Dr. Markus Weinberger, HS Aalen
 * 
 * This sketch is based on the follwing sources
 * http://osoyoo.com/2016/12/02/nodemcu-light-sensor-mqtt/
 * https://developer.ibm.com/recipes/tutorials/connect-an-esp8266-with-the-arduino-sdk-to-the-ibm-iot-foundation/
 * For the callback function: https://tuts.codingo.me/connect-esp8266-to-ibm-bluemix
 */

//Include required libraries
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//Set analog input A0 as "Temp_sensor"
#define Temp_sensor A0

//ENTER YOUR DATA HERE - Below are the device data from the IBM platform
#define ORG "o8j7sq"
#define DEVICE_TYPE "ESP8266"
#define DEVICE_ID "ESP"
#define TOKEN "aqCfLJtztLQ8!a_VHu" // This is the device authentication token, that you saw when creating the device in the IoT Platform

//ENTER YOUR DATA HERE - Data for the WLAN connection
const char* ssid = "crfb"; //mein Handy als Hotspot
const char* password = "CS10Ok09"; //mein Handy als Hotspot

//Here data for connection to IBM platform is being defined
char server[] = ORG ".messaging.internetofthings.ibmcloud.com"; //URL of the MQTT broker, see here for reference: https://console.bluemix.net/docs/services/IoT/reference/security/connect_devices_apps_gw.html#connect_devices_apps_gw
char topicdata[] = "iot-2/evt/status/fmt/json"; //The topic to which the device publishes its data. - event_id is "status". See here for reference: https://console.bluemix.net/docs/services/IoT/devices/mqtt.html#publishing_events
char topiccmd [] = "iot-2/cmd/test/fmt/String"; //The topic to which the device listens. command_id (here "test") has to fit to the data in Node-Red IBMIoT output, e. g. Command Type "test", Format "String" - see here for MQTT-Topic format: https://console.bluemix.net/docs/services/IoT/devices/mqtt.html#subscribing_to_commands 
char authMethod[] = "use-token-auth"; //This going to be the user name for connecting the MQTT-Broker. See here: https://console.bluemix.net/docs/services/IoT/reference/security/connect_devices_apps_gw.html#device-authentication
char token[] = TOKEN; //This is going to be the password for connecting the MQTT-Broker. See here: https://console.bluemix.net/docs/services/IoT/reference/security/connect_devices_apps_gw.html#device-authentication
char clientId[] = "d:" ORG ":" DEVICE_TYPE ":" DEVICE_ID; //See here for reference: https://console.bluemix.net/docs/services/IoT/reference/security/connect_devices_apps_gw.html#mqtt_authentication
boolean led = false; //This keeps the info, whether Led is on or off

void callback(char* topiccmd, byte* payload, unsigned int payloadLength); // Has to be present before contructor of PubSubClient, otherwise it is not compiled.

// Initialization
WiFiClient espClient; //Creates a WifiClient called "espClient"
PubSubClient client(server, 1883, callback, espClient); //Constructor for the MQTT-Client; Function "callback" called, when a message arrives on a subscribed topic. See here for reference: https://pubsubclient.knolleary.net/api.html#PubSubClient2
//Variables are defined
long lastMsg = 0;
int value = 0;

//Function setup_wifi connects to the specified WLAN network and provides outputs on the console.
void setup_wifi() {
   delay(100);
  // We start by connecting to a WiFi network
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password); //Here the actual connection is established. See here for reference: https://www.arduino.cc/en/Reference/WiFiBegin
    while (WiFi.status() != WL_CONNECTED) //Checks the status of the WLAN (Wifi) connection. Reference: https://www.arduino.cc/en/Reference/WiFiStatus
    {
      delay(500);
      Serial.print(".");
    }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());//Wifi.localIP returns the IP address. Reference: https://www.arduino.cc/en/Reference/WiFiLocalIP
}


// Connect or reconnect to MQTT Broker
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    if (client.connect(clientId, authMethod, token)) //Here the connection is actually established. The user name is set to <authMethod>, see above, and the password is set to <token>
    {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc="); // See http://pubsubclient.knolleary.net/api.html#state for the failure code and its reason
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 6 seconds before retrying
      delay(6000);
    }
    initManagedDevice();//Calls the function below
  }
} //end reconnect()

void initManagedDevice() {
  if (client.subscribe(topiccmd)) {  //Subscribes to an MQTT topic defined in topiccmd. See here: https://pubsubclient.knolleary.net/api.html#subscribe
    Serial.println("subscribe to cmd OK");
  } else {
    Serial.println("subscribe to cmd FAILED");
  }
}

void callback(char* topic, byte* payload, unsigned int payloadLength) { //This is the callback function, which is called when a message on the subscribed topic arrives. See here: https://pubsubclient.knolleary.net/api.html#callback
  Serial.print("callback invoked for topic: "); 
  Serial.println(topic);

  for (int i = 0; i < payloadLength; i++) { //payload is a byte array. The payload is printed on the console
    Serial.print((char)payload[i]);
  }
  Serial.println();

  //toggling the led for any payload which is received on the subscribed topic
  led = !led;
  if (led) {
    digitalWrite(13, HIGH);
    Serial.println("LED an");
  } else {
    digitalWrite(13, LOW);
    Serial.println("LED aus");
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(13, OUTPUT); //GPIO13 which refers to D7 on the board is set as an output
  setup_wifi();
}


void loop() {
  if (!client.connected()) {  //reconnect, if connection to MQTT broker is lost
    reconnect();
  }
  
  client.loop();  //keep MQTT connection alive
  
  long now = millis(); //Variable now is set to the actual time (milliseconds since the board started
// Following line defines a minimal intervall for new messages
  if (now - lastMsg > 500) { 
     lastMsg = now;

    // Read temperature Value
    int val=analogRead(Temp_sensor); //Variable val is set to the reading of A0 to which the light sensor is connected
    val = (val - 32) * 5/9;  // convert temp in farenheit to celsius
    // Build payload - see here for reference: https://developer.ibm.com/recipes/tutorials/connect-an-esp8266-with-the-arduino-sdk-to-the-ibm-iot-foundation/
    String payload = "{\"d\":{\"myName\":\"";
      payload += DEVICE_TYPE;
      payload += ".";
      payload += DEVICE_ID;
      payload += "\",\"temperature\":";
      payload += val;
      payload += ",\"ledstatus\":";
      payload += led;
      payload += "}}";

   // Publish payload and generate output to serial monitor
   Serial.print("Sending payload: ");
   Serial.println(payload);
 
   if (client.publish(topicdata, (char*) payload.c_str())) {
     Serial.println("Publish ok");
   } else {
     Serial.println("Publish failed");
   }

   // Wait until next message is being built and send - see also the minimal interval in the beginning of loop
   delay(2000);

  }
}
